import React from 'react';
import './App.css';

function App() {
  const element = React.createElement(
    'h2', {}, 'Kami dari kelompok FE-03'
  );

  const namaKelompok = ['tio', 'zufar', 'aisyah', 'adi', 'iskandar'];

  const input = <input type="text" />

  return (
    <div className="app">
      <header className="App-header" >
        <h1>Hello World!!</h1>
        <p>Aplikasi React ini dibuat dengan create - react - app</p>
      </header>

      <div className="perkenalan">
        <h2>Hallo</h2>
        {element}

        {namaKelompok.map(function (nama) {
          return <p>{nama}</p>
        })}


        {input}
      </div>
    </div>
  );
}

export default App;